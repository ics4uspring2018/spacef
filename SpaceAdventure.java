
/**
 * This is the Space Adventure Game.
 *
 * Brice Zhao
 * 2/14/2018
 */
import java.util.Scanner;
import java.util.Random;

public class SpaceAdventure
{
    PlanetarySystem planetarySystem;
    public SpaceAdventure()
    {
       Planet[] planets = new Planet[8]; 
       planetarySystem = new PlanetarySystem("Solar System", planets);
       
       Planet mercury = new Planet ("Mercury", "A very hot planet, closest to the sun.");
       Planet neptune = new Planet ("Neptune", "A very cold planet, furthest from the sun.");
       Planet venus = new Planet ("Venus", "Like the brand.");
       Planet earth = new Planet ("Earth", "Humans live here.");
       Planet mars = new Planet ("Mars", "Use to have life here.");
       Planet jupiter = new Planet ("Jupiter", "A gas giant.");
       Planet saturn = new Planet ("Saturn", "Has a big ring.");
       Planet uranus = new Planet ("Uranus", "Funny name.");
       Planet catLand = new Planet ("Cat Land", "Meow Meow.");
  
       planetarySystem.planets[0] = mercury;
       planetarySystem.planets[1] = neptune;
       planetarySystem.planets[2] = venus;
       planetarySystem.planets[3] = earth;
       planetarySystem.planets[4] = mars;
       planetarySystem.planets[5] = jupiter;
       planetarySystem.planets[6] = saturn;
       planetarySystem.planets[7] = uranus;
       planetarySystem.planets[8] = catLand;
    }
    public static void main()
    {
        SpaceAdventure adventure = new SpaceAdventure();
        adventure.start();
    }
    private void start()
    {
        displayIntroduction();
        greetAdventurer();
        if(!(planetarySystem.planets == null || planetarySystem.planets.length == 0))
        {
            determineDestination();
        }

    }
    private void displayIntroduction()
    {
        int planets = 8;
        double circumference = 28859.82;
        System.out.println("Welcome to the " + planetarySystem.name + "!");
        System.out.println("There are " + planetarySystem.planets.length + " planets to  explore.");
        System.out.println("You are currently on Earth, which has a circumference of " + circumference + " miles.");
    }
    private void greetAdventurer()
    {
        System.out.println("What is your name?");
        Scanner a = new Scanner(System.in);
        String name = a.nextLine();
        System.out.println("Nice to meet you, " + name + ". My name is Eliza.");
    }
    private void visit(String planetName)
   {       
        System.out.println("Travelling to " + planetName + "..."); 
        {
           for( Planet planet: planetarySystem.planets)
               {
               if(planetName.equals(planet.name))
                    System.out.println("Arrived at " + planet.name + ". " + planet.description);
               
               }
          
         }             
   }
   private static String responseToPrompt(String prompt) 
   {
       Scanner scan = new Scanner(System.in); 
       System.out.println(prompt);
       return scan.nextLine();
   }
    private void determineDestination()
    {
        System.out.println("Let's go on an adventure!  Shall I randomly choose a planet for you to visit? ");
        while(true)
        {
            Scanner b = new Scanner(System.in);
            String travel = b.nextLine();
            if(travel.equals("Y"))
            {
                int upperBound = planetarySystem.planets.length;
                Random rng = new Random();
                int index = rng.nextInt(upperBound);    
                visit(planetarySystem.planets[index].name);

                break;
            }
            if(travel.equals("N"))
            {
                String planetName = responseToPrompt("Ok, name the planet you would  like to visit.");
                visit(planetName);
                break;
            }
            else
            {
                System.out.println("Huh?  Sorry, I didn't get that.");
            }
        }
    }
}
/**
 * Q1 The length of the planet array
 * Q2 Give values to each array slot
 * Q3 Yes
 * Q4 We would extend the array length
 * Q5 It means to organize it by grouping similar commands
 * Q6 Ask them to input a name, and we can cross reference with every planet name in our array
 * Q7 After we match their input with a planet, we give the description of the same planet
 * Q8 It'll be invalid
 * Q9 We can check for it in the beginning
 * Q10 
 */