
/**
 * This is the PlanetarySystem.
 *
 * Brice Zhao
 * 2/14/2018
 */
import java.util.Random;

public class PlanetarySystem
{
    String name; 
    Planet[] planets;
    public PlanetarySystem (String name, Planet[] planets)
    {
        this.name = name;
        this.planets = planets;
    }
    Planet randomPlanet()
    {
        Random rng = new Random();
        if (!(planets == null || planets.length == 0))
        {
            int index = rng.nextInt(planets.length);
            return planets[index];
        }
        else
        {
            System.out.println("Sorry, but there are no planets in this system.");
            
        }

        
        return null;
        
    }
}
