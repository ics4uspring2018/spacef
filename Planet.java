
/**
 * This is a Planet.
 *
 * Brice Zhao
 * 2/14/2018
 */
public class Planet
{
    String name;
    String description;
    
    public Planet (String name, String description)
    {
        this.name = name;
        this.description = description;
    }
}
